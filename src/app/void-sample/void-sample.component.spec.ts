import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoidSampleComponent } from './void-sample.component';

describe('VoidSampleComponent', () => {
  let component: VoidSampleComponent;
  let fixture: ComponentFixture<VoidSampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoidSampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoidSampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
