import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';

@Component({
  selector: 'app-void-sample',
  templateUrl: './void-sample.component.html',
  styleUrls: ['./void-sample.component.css'],
  animations: [
    trigger('heroState', [
      state('inactive', style({
        backgroundColor: 'red',
        transformOrigin: 'center',
        transform: 'scale(1)'
      })),
      state('active', style({
        backgroundColor: 'green',
      })),
      // A void state is a element in a detached state.
      // void => * transition happens when a detached element cames into any other states.
      // * <= void is exactly reverse of the previous one
      transition('void <=> *', [animate('3000ms ease-in', style({opacity: 0 }))]),
    ])
  ]
})
export class VoidSampleComponent implements OnInit {
  showMario: Boolean = false;
  mario = 'http://172.24.144.20/trial/angular/angular-animation/src/assets/mario1.png';
  transitionState: string = 'active';

  toggleState() {
    this.transitionState = this.transitionState === 'inactive' || this.transitionState === '' ? 'active' : 'inactive';
  }

  ngOnInit() {
  }

  displayMario(bool: boolean) {
    this.showMario = bool;
  }

}
