interface AnimatePosition {
    xpos: number;
    ypos: number;
}

export let pos : AnimatePosition = {xpos: 0, ypos: 0};
