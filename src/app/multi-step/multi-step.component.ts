import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';
import { pos } from './animation-position';

@Component({
  selector: 'app-multi-step',
  templateUrl: './multi-step.component.html',
  styleUrls: ['./multi-step.component.css'],
  animations: [
    trigger('heroState', [
      state('inactive', style({
        backgroundColor: 'red',
        transform: 'translate( 700px, 0px)'
      })),
      state('active', style({
        backgroundColor: 'green',
      })),
      transition('active => inactive', [
        // Providing multiple patterns of same kind of animations on a single transition
        animate('3000ms ease-in', keyframes([
          style({opacity: 1, transform: 'translate( 0px, 0px)'}),
          style({opacity: 1, transform: 'translate( 175px, -175px)'}),
          style({opacity: 1, transform: 'translate( 350px, 0px)'}),
          style({opacity: 1, transform: 'translate( 525px, -175px)'}),
          style({opacity: 1, transform: 'translate( 700px, 0px)'})
        ])),
      ])
    ]
  )]
})

export class MultiStepComponent implements OnInit {
  showItem: Boolean = false;
  state: string = 'active';
  mario: string = 'http://172.24.144.20/trial/angular/angular-animation/src/assets/mario1.png';
  constructor() { }

  ngOnInit() {
  }

  toggleState() {
    this.state = this.state === 'inactive' || this.state === '' ? 'active' : 'inactive';
  }

  displayItem() {
    this.showItem = true;
  }

}
