import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SimpleComponent } from './simple/simple.component';
import { VoidSampleComponent } from './void-sample/void-sample.component';
import { MultiStepComponent } from './multi-step/multi-step.component';
import { ParallelComponent } from './parallel/parallel.component';
import { AppRoutingModule } from './/app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    SimpleComponent,
    VoidSampleComponent,
    MultiStepComponent,
    ParallelComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
