import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-simple',
  templateUrl: './simple.component.html',
  styleUrls: ['./simple.component.css'],
  animations: [
    trigger('heroState', [
      state('moved', style({
        transform: 'translateX(600px)'
      })),
      state('idle', style({
        transform: 'translateX(0px)'
      })),
      transition('moved <=> idle', animate('1000ms ease-in'))
    ])
  ]
})

export class SimpleComponent implements OnInit {
  title = 'Angular Animation';
  mario = 'http://172.24.144.20/trial/angular/angular-animation/src/assets/mario1.png';
  transitionState: string = 'idle';
  changeState() {
    this.transitionState = this.transitionState === 'moved' ? 'idle' : 'moved';
    console.log(this.transitionState);
  }

  ngOnInit() {
  }

  animationDone() {
    alert('Animation has been completed');
  }
}
