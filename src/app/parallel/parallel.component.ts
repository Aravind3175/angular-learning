import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition, group } from '@angular/animations';

@Component({
  selector: 'app-parallel',
  templateUrl: './parallel.component.html',
  styleUrls: ['./parallel.component.css'],
  animations: [
    trigger('heroState', [
      state('inactive', style({
        backgroundColor: 'red',
        transform: 'scale(1)'
      })),
      state('active', style({
        backgroundColor: 'green',
        transform: 'scale(1.5)'
      })),
      transition('active => inactive', [
        // Changin multiple animations on a same object on a single transition
        group([
          animate('10s 0.1s ease', style({
            backgroundColor: 'red'
          })),
          animate('10s ease', style({
            opacity: '0.3'
          }))
        ])
      ])
    ]
  )]
})

export class ParallelComponent implements OnInit {
  showItem: Boolean = false;
  transitionState: string = 'active';
  mario = 'http://172.24.144.20/trial/angular/angular-animation/src/assets/mario1.png';
  constructor() { }

  ngOnInit() {
  }

  toggleState() {
    this.transitionState = this.transitionState === 'inactive' || this.transitionState === '' ? 'active' : 'inactive';
  }

  displayItem() {
    this.showItem = true;
  }
}
