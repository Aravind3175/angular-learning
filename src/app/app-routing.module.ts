import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SimpleComponent } from '../app/simple/simple.component';
import { VoidSampleComponent } from '../app/void-sample/void-sample.component';
import { MultiStepComponent } from '../app/multi-step/multi-step.component';
import { ParallelComponent } from '../app/parallel/parallel.component';

const routes: Routes = [
  { path: 'simple', component: SimpleComponent },
  { path: 'void', component: VoidSampleComponent },
  { path: 'multi-step', component: MultiStepComponent },
  { path: 'parallel', component: ParallelComponent },
  { path: '', component: SimpleComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
